package com.reservation.model;

import javax.persistence.*;

@Entity
@Table(name = "apartment")
public class Apartment {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column
    private Long id;

    @Column
    private Integer price;

    @Column
    private Integer num_of_people;

    @Column
    private String room_description;

    public Apartment() {
    }

    public Apartment(Integer price, Integer num_of_people, String room_description) {
        this.price = price;
        this.num_of_people = num_of_people;
        this.room_description = room_description;
    }

    public Long getId() {
        return id;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getNum_of_people() {
        return num_of_people;
    }

    public String getRoom_description() {
        return room_description;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public void setNum_of_people(Integer num_of_people) {
        this.num_of_people = num_of_people;
    }

    public void setRoom_description(String room_description) {
        this.room_description = room_description;
    }
}
