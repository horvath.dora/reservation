package com.reservation.service;

import com.reservation.model.Reservation;
import com.reservation.dao.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ReservationService {

    private final ReservationRepository reservationRepository;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    public List<Reservation> selectAllReservations(){
        return reservationRepository.findAll();
    }

    public Optional<Reservation> getReservationById(Long id){
        return reservationRepository.findById(id);
    }

    public void addReservation(Reservation reservation){
         reservationRepository.save(reservation);
    }

    public void deleteReservationById(Long id){
         reservationRepository.deleteById(id);
    }

    public void updateReservationById(Reservation reservation){
        reservationRepository.save(reservation);
    }

}
