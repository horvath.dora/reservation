package com.reservation.service;

import com.reservation.dao.ApartmentRepository;
import com.reservation.model.Apartment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ApartmentService {

    private final ApartmentRepository apartmentRepository;

    @Autowired
    public ApartmentService(ApartmentRepository apartmentRepository) {
        this.apartmentRepository = apartmentRepository;
    }

    public List<Apartment> selectAllApartments(){
        return apartmentRepository.findAll() ;
    }

    public void addApartment(Apartment apartment){
        apartmentRepository.save(apartment);
    }

    public Optional<Apartment> getApartmentById(Long id){
        return apartmentRepository.findById(id);
    }

    public void deleteApartmentById(Long id){
         apartmentRepository.deleteById(id);
    }

    public void updateApartmentById(Apartment apartment){
        apartmentRepository.save(apartment);
    }

}
