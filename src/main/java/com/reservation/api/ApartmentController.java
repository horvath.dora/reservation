package com.reservation.api;

import com.reservation.model.Apartment;
import com.reservation.service.ApartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/apartment")
public class ApartmentController {

    private final ApartmentService apartmentService;

    @Autowired
    public ApartmentController(ApartmentService apartmentService) {
        this.apartmentService = apartmentService;
    }

    @GetMapping
    public List<Apartment> selectAllApartments(){
        return apartmentService.selectAllApartments();
    }

    @PostMapping
    public void addApartment(@RequestBody Apartment apartment){
        apartmentService.addApartment(apartment);
    }

    @GetMapping("/{id}")
    public Optional<Apartment> getApartmentById(@PathVariable("id") Long id){
        return apartmentService.getApartmentById(id);
    }

    @PutMapping("/{id}")
    public void updateApartmentById(@PathVariable("id") Long id, Apartment apartment){
        apartmentService.updateApartmentById(apartment);
    }

    @DeleteMapping("/{id}")
    public void deleteApartmentById(@PathVariable("id") Long id){
        apartmentService.deleteApartmentById(id);
    }

}
